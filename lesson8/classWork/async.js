/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

document.addEventListener('DOMContentLoaded', function(){

	let div = document.createElement('div');
	
	div.id = "async";

	div.innerHTML = `
	<div><h1>Company</h1><ul id="company"></ul></div>
	<div><h1>Balance</h1><ul id="balance"></ul></div>
	<div><h1>Show registration date</h1><ul id="data"></ul></div>
	<div><h1>Show address</h1><ul id="adress"></ul></div>
	`;

	document.body.appendChild(div);

	let url = 'http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2',
	company = document.getElementById('company'),
	balance = document.getElementById('balance'),
	data = document.getElementById('data'),
	adress = document.getElementById('adress');

	const ConvertToJSON = data => data.json();

	async function getData(){

		const dataObj = await fetch(url),
		dataJson = ConvertToJSON(dataObj);
		return dataJson;
	}

	const getCompany = getData();

	getCompany.then( response => {
		console.log(response)
		response.map( item => {

			let li = document.createElement('li'),
			li2 = document.createElement('li'),
			li3 = document.createElement('li'),
			li4 = document.createElement('li'),
			button = document.createElement('button'),
			button2 = document.createElement('button');

			li.innerHTML = item.company;
			company.appendChild(li);
			
			li2.innerHTML = item.balance;
			balance.appendChild(li2);

			button.innerText = 'Show';
			li3.appendChild(button);
			data.appendChild(li3);

			button2.innerText = 'Show';
			li4.appendChild(button2);
			adress.appendChild(li4);

			button.addEventListener('click', event =>{
				let enter = event.target.closest('li');
				enter.innerHTML = item.registered;
			});

			button2.addEventListener('click', event =>{
				let enter = event.target.closest('li');

				enter.innerHTML = '';
				for ( let elem in item.address) {
					
					let span = document.createElement('span')
					span.innerHTML = `
					${elem}: ${item.address[elem]}, 
					`;
					enter.appendChild(span);
				}
			});
		})
	})

})