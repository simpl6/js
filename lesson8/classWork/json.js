
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

*/

document.addEventListener('DOMContentLoaded', function(){

	let div = document.createElement('div');
	
	div.innerHTML = `
	<form action="" id="enter">
	<span>Name: </span>
	<input type="text" name="name">
	<span>Login: </span>
	<input type="text" name="login">
	<span>Password: </span>
	<input type="password" name="password">
	<button name="toJSON">To JSON</button>
	</form>

	<div id="result"></div>

	<form action="" id="enter2">
	<span>String JSON: </span>
	<input type="text" name="json">
	<button name="fromJSON">From JSON</button>
	</form>

	<div id="result2"></div>
	`;
	document.body.appendChild(div);

	let result = document.getElementById('result'),
	result2 = document.getElementById('result2'),
	enter = document.getElementById('enter'),
	enter2 = document.getElementById('enter2'),
	stringJSON = '',
	user = function( name, login, password ){
		this.name = name;
		this.login = login;
		this.password = password;
	};

	enter.toJSON.addEventListener('click', event => {
		let userNew = new user(enter.name.value, enter.login.value, enter.password.value);
		stringJSON = JSON.stringify( userNew );
		result.innerHTML = stringJSON;
		enter.name.value = '';
		enter.login.value = '';
		enter.password.value = '';
		enter2.json.value = stringJSON;
		event.preventDefault();
	});
	enter2.fromJSON.addEventListener('click', event => {
		let objectJSON = JSON.parse( enter2.json.value );
		result2.innerHTML = 'Watch result in console !';
		console.log(objectJSON);
		event.preventDefault();
	});




});