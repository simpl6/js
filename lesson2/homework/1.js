
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
var button = document.querySelectorAll('.showButton'),
    tab = document.getElementsByClassName('tab');

button.forEach(function(item) {item.addEventListener('click',showTab);});

function showTab(event){
      
      var dataButton = event.target.dataset.tab;

      hideAllTabs();

      for (var i=0; i<tab.length; i++){

        if (tab[i].dataset.tab == dataButton){
          tab[i].classList.add('active');
        }
      } 
}

function hideAllTabs(){
    for (var i=0; i<tab.length; i++){
      
        if (tab[i].classList.contains('active')){
          tab[i].classList.remove('active');
        }
    }
}