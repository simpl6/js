document.addEventListener('DOMContentLoaded', function(){

	let inputNewsAuthor = document.getElementById('author'),
	inputlink = document.getElementById('link'),
	buttonSendNews = document.getElementById('sendNews'),
	textareaGetNews = document.getElementById('getNews'),
	divImagePreview = document.getElementById('imagePreview'),
	divCommentConstructor = document.querySelector('.commentConstructor'),
	arrayNews = [],
	count = 0;

	inputlink.addEventListener('input', event => {
		divImagePreview.style.background = `url(${inputlink.value})`;
		divImagePreview.style.backgroundSize = `cover`;
		})
	
	const timeDate = check => {
    let day = new Date();
    	if(check !== true){
    		return `${day.getDate()}.${day.getMonth()+1}.${day.getFullYear()} ${day.getHours()}:${day.getMinutes()}`;
    	} else {
    		return `${day.getDate()}.${day.getMonth()+1}.${day.getFullYear()}`;
		}
  	}

	class News {
    	constructor(){
    		this.id = count;
    		this.comments = 0;
    		this.arrayComments = [];
    	  	this.likes = 0;
    	  	this.date = timeDate(true);
    	  	this.author = inputNewsAuthor.value;
    	  	this.text = textareaGetNews.value;
    	  	this.url = inputlink.value; 
      	}
      	likePlus(){
      		this.likes += 1;
    	}
    	showComments(){
      		this.comments += 1;
    	}
	}

	class Comments {
		constructor(id, author, text){
			this.id = id;
			this.date = timeDate();
			this.author = author;
			this.text = text;
		}
	}

	buttonSendNews.addEventListener('click', () => {
		arrayNews[count] = new News();
		releaseNews(arrayNews[count]);
		count++;

		inputNewsAuthor.value = '';
		textareaGetNews.value = '';
		inputlink.value = '';
		divImagePreview.style.background = '';

		event.preventDefault();
	})

	function releaseNews(obj) {
		
		let divNewsFeed = document.createElement('div'),
		divFeed = document.querySelector('.feed'),
		authorDefault = '';

		divNewsFeed.classList.add('newsFeed');

		if(obj.author === ''){
			authorDefault = 'Anonymous';
		} else {
			authorDefault = obj.author;
		}

		divNewsFeed.innerHTML = `
			<div class="authorFeed">${authorDefault}</div>
				<div class="dataFeed">${obj.date}</div>
				<div class="textFeed">${obj.text}</div>
				<div>
					<img src="${obj.url}" alt="">
				</div>
				<div class="feedBack">
					<span class="spanLike">Likes: ${obj.likes}</span>
					<button class="like"><img src="https://cdn4.iconfinder.com/data/icons/evil-icons-user-interface/64/like-48.png" alt=""></button>
					<span class="spanShowComments">Show comments (${obj.comments})</span>
					<button class="addComments">Write comments</button>
			</div>
		`;
		divFeed.appendChild(divNewsFeed);

		let buttonLikes = divNewsFeed.querySelector('.like'),
		spanLike = divNewsFeed.querySelector('.spanLike'),
		spanShowComments = divNewsFeed.querySelector('.spanShowComments'),
		addComments = divNewsFeed.querySelector('.addComments');
		
		buttonLikes.addEventListener('click', function(){
        	obj.likePlus.call(obj);
        	spanLike.innerHTML = `Likes: ${obj.likes}`;
      	});

		addComments.addEventListener('click', function(){
        	let countShowComments = obj.showComments.bind(obj);
        	
        	divCommentConstructor.style.backgroundColor = 'white';
        	divCommentConstructor.innerHTML = `
				<form action="">
					<h3>Comment constructor</h3>
					<input type="text" placeholder="Comment author" class="CommentAuthor">
					<textarea placeholder="Text" class="getComment"></textarea>
					<button class="sendComment">Send comment</button>
				</form>
        	`
        	releaseComments(obj.id, countShowComments, obj, spanShowComments, divNewsFeed)
        	obj.id++;
      	});
	}

	function releaseComments(index, metodCommentsPlus, obj, spanShowComments, divNewsFeed) {

		let	buttonSendComment = divCommentConstructor.querySelector('.sendComment'),
			inputCommentAuthor = divCommentConstructor.querySelector('.CommentAuthor'),
			textareaGetComments = divCommentConstructor.querySelector('.getComment');

		buttonSendComment.addEventListener('click', event => {

			obj.arrayComments[index] = new Comments(index, inputCommentAuthor.value, textareaGetComments.value);
			
			let divCommentsFeed = document.createElement('div'),
			authorDefault = '';

			if(obj.arrayComments[index].author === ''){
				authorDefault = 'Anonymous';
			} else {
				authorDefault = obj.arrayComments[index].author;
			}

			divCommentsFeed.classList.add('commentsFeed');
			divCommentsFeed.innerHTML = `
				<div class="container">
						<div class="authorFeed">${authorDefault}</div>
						<div class="dataFeed">${obj.arrayComments[index].date}</div>
				</div>
				<div class="textFeed">${obj.arrayComments[index].text}</div>
			`;
			divNewsFeed.appendChild(divCommentsFeed);


			metodCommentsPlus();
			spanShowComments.innerHTML = `Show comments (${obj.comments})`;
			divCommentConstructor.style.backgroundColor = 'silver';
			divCommentConstructor.innerHTML = '';

			event.preventDefault();
		})
		
	}

})