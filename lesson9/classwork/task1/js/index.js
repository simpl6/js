/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */
import fly from './fly';
import swim from './swim';
import hunt from './hunt';
import mail from './mail';

function bird(name){
  this.name = name;
  this.canFly = function(){
    fly(this.name);
  };
  this.canSwim = function(){
    swim(this.name);
  };
  this.canHunt = function(){
    hunt(this.name);
  };
  this.canMail = function(){
    mail(this.name);
  };
}

let seagull = new bird('Seagull');

seagull.canFly();
seagull.canSwim();
seagull.canHunt();

console.log('--------------------------------------------------')

let pigeon = new bird('Pigeon');

pigeon.canFly();
pigeon.canMail();
